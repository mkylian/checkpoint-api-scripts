#!/usr/bin/env python3
# pylint: disable=C0103, R0903, W0603
"""
This script fetches list of host (ip address per line) and push hosts as \
objects into defined group object throught checkpoint API
"""

import config as cfg
import json
import requests

def api_call(command, json_payload):
    """
    Call API function returns response data and set response \
    status code variable
    """

    url = 'https://' + cfg.IP_ADDR + ':' + cfg.PORT + '/web_api/' + command
    global sid, response_status_code
    if sid is None:
        request_headers = {'Content-Type' : 'application/json'}
    else:
        request_headers = {'Content-Type' : 'application/json', 'X-chkp-sid' : sid}
    r = requests.post(url, data=json.dumps(json_payload), headers=request_headers)
    response_status_code = r.status_code
    return r.json()


def login(user, password):
    """
    Make API login and return SID
    """
    payload = {'user':user, 'password' : password}
    response = api_call('login', payload)
    return response["sid"]

class bcolors:
    """
    Set terminal font colors
    """
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


# Initialize
sid = None
response_status_code = None
create_group_object = None
purged_list = []

# Fetch and read data
fetched_hosts = [line.rstrip('\n') for line in open("input.txt", "r")]

# Login and set session name
sid = login(cfg.USER, cfg.PASSWORD)
print("session id: " + sid)
request_data = {'new-name':'API job'}
request_result = api_call('set-session', request_data)


# Fetch network group members
request_data = {'name':cfg.GROUP}
request_result = api_call('show-group', request_data)

# If group object does not exist, create it later
if response_status_code == 404:
    create_group_object = True
# Group object exists, process current member hosts
if response_status_code == 200 and request_result['members'] != []:
    existing_hosts = request_result['members']
    print("Network group " + cfg.GROUP)
    purge_hosts_data_set = {'name':cfg.GROUP, 'members':{'remove':[]}}
    for member in existing_hosts:
        # If object already exists, don't create it
        if member['ipv4-address'] in fetched_hosts:
            fetched_hosts.remove(member['ipv4-address'])
            print(member['name'] + bcolors.OKGREEN + " already exists" + bcolors.ENDC)
        # If object is not in fetched hosts, remove it from network group object
        else:
            print(member['name'] + bcolors.FAIL + " purging" + bcolors.ENDC)
            purged_list.append(member['name'])
            purge_hosts_data_set['members']['remove'].append(member['name'])
    request_result = api_call('set-group', purge_hosts_data_set)
    # Then delete hosts
    for member in purged_list:
        request_data = {'name':member}
        request_result = api_call('delete-host', request_data)

# If group object does not exist, create it
if create_group_object:
    # No error check
    request_data = {'name':cfg.GROUP}
    api_call('add-group', request_data)

# Add new hosts to network group
add_hosts_data_set = {"name":cfg.GROUP, "members": {"add":[]}}
for add_host_ip in fetched_hosts:
    add_host_name = "host_" + add_host_ip
    add_hosts_data_set['members']['add'].append(add_host_name)
    request_data = {'name':add_host_name, 'ip-address':add_host_ip, 'comments':cfg.HOST_COMMENT}
    request_result = api_call('add-host', request_data)
    if response_status_code == 200:
        print("host_" + str(request_data['ip-address']) + bcolors.OKGREEN \
        + " created sucessfully" + bcolors.ENDC)
    if response_status_code >= 400:
        if request_result['errors'] != []:
            print("host_" + str(request_data['ip-address']) + " " + bcolors.WARNING \
            + str(request_result['errors'][0]['message']) + bcolors.ENDC)
        else:
            print("host_" + str(request_data['ip-address']) + " " + bcolors.WARNING \
            + str(request_result['warnings'][0]['message']) + bcolors.ENDC)

request_result = api_call('set-group', add_hosts_data_set)

publish_result = api_call("publish", {})
print("publish result: " + json.dumps(publish_result))

logout_result = api_call("logout", {})
print("logout result: " + json.dumps(logout_result))
