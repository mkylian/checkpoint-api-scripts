#!/usr/bin/env python3
# pylint: disable=C0103, R0903, W0603

"""
Config file
"""

# Login user
USER = ''

# Login password
PASSWORD = ''

# IP or hostname of management API
IP_ADDR = ''

# Management API port, 443 by default
PORT = ''

# Custom network group object to work with
GROUP = ''

# Custom comment for created hosts
HOST_COMMENT = ''

